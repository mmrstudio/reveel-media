<?php
/**
 * Single Event Template
 * A single event. This displays the event title, description, meta, and
 * optionally, the Google map for the event.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/single-event.php
 *
 * @package TribeEventsCalendar
 * @version 4.6.3
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

$events_label_singular = tribe_get_event_label_singular();
$events_label_plural   = tribe_get_event_label_plural();

$event_id = get_the_ID();

?>

<div id="tribe-events-content" class="tribe-events-single">

	<p class="tribe-events-back">
		<a href="<?php echo esc_url( tribe_get_events_link() ); ?>"> <?php printf( '&laquo; ' . esc_html_x( 'All %s', '%s Events plural label', 'the-events-calendar' ), $events_label_plural ); ?></a>
	</p>

	<!-- Notices -->
	<?php tribe_the_notices() ?>
	<!-- <div class="tribe-events-schedule tribe-clearfix">
		<?php //echo tribe_events_event_schedule_details( $event_id, '<h2>', '</h2>' ); ?>
		
	</div> -->

	<!-- #tribe-events-header -->

	<?php while ( have_posts() ) :  the_post(); ?>
		<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<div class="col-md-9 event-left">

				<!-- Event featured image, but exclude link -->
			<?php echo tribe_event_featured_image( $event_id, 'full', false ); ?>

			<!-- Event content -->
			<?php do_action( 'tribe_events_single_event_before_the_content' ) ?>
			<div class="tribe-events-single-event-description tribe-events-content">

					<?php $content = get_the_content();
						if(!empty($content)) {  ?>

						<h2 class="singleevent-title"> Event Description </h2>

					<?php	}  ?>
				
				<?php the_content(); ?>
			</div>
			
			</div>
			<div class="col-md-3 event-right">
		
			<!-- Event meta -->
			<?php do_action( 'tribe_events_single_event_before_the_meta' ) ?>
			<?php tribe_get_template_part( 'modules/meta' ); ?>
		
			<?php //do_action( 'tribe_events_single_event_after_the_meta' ) ?>
			<!-- .tribe-events-single-event-description -->
			<?php do_action( 'tribe_events_single_event_after_the_content' ) ?>
			
			<hr/>
				<?php  $ticket = get_field('buy_ticket_link');

						if(!empty($ticket)) {  ?>

					<a href="<?php echo $ticket;?>" target="_blank" class="buy-tickets"> Buy Tickets </a>


						<?php }

				?>
				<?php //echo do_shortcode('[addtoany]' );?>
			</div>
		</div> <!-- #post-x -->
		<?php if ( get_post_type() == Tribe__Events__Main::POSTTYPE && tribe_get_option( 'showComments', false ) ) comments_template() ?>
	<?php endwhile; ?>

	<!-- Event footer -->
	
	<!-- #tribe-events-footer -->

</div><!-- #tribe-events-content -->
