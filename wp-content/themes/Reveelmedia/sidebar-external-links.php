    <div id="sidebar" class="col-sm-3 col-md-3">

    <?php if ( is_active_sidebar( 'main-sidebar' ) ) : ?>
        <?php dynamic_sidebar( 'main-sidebar' ); ?>
    <?php endif; ?>


  
    <?php $external_links = get_fields("external_links","options"); ?>

<?php if( have_rows('external_links', 'options') ):?>
    <ul class="buttons-box">
    <?php while ( have_rows('external_links', 'options') ) : the_row(); ?>
        <li>
            <a href="<?php the_sub_field('url', 'options'); ?>">
                <span><?php the_sub_field('text', 'options'); ?></span>
                <i class="fa fa-chevron-circle-right"></i>
            </a>
        </li>
    <?php endwhile; ?>
    </ul>   
<?php endif; ?>
</div>

