jQuery(document).ready(function($){
if ($(window).width() <= 768) {
	$('.solution-section').slick({
		infinite: true,
		speed: 2000,
		arrows: false,
		autoplay: true,
		slidesToShow:1,
		slidesToScroll: 1
	});

	$('.testimonial').slick({
		infinite: true,
		arrows: false,
		autoplay: true,
		speed: 2000,
		fade: true,
		cssEase: 'linear'
	});
}


$('.slides').slick({

   slidesToShow: 3,
   slidesToScroll: 1,
   autoplay: true,
   autoplaySpeed: 5000,
    
  
   responsive: [
    		{
		      breakpoint: 1200,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 1,
		        infinite: true,
		        dots: false
		      }
		    },
		    {
		      breakpoint: 769,
		      settings: {
		        slidesToShow: 1,
		        autoplay: false,
		        slidesToScroll: 1,
		        infinite: true,
		        dots: false
		      }
		    },
		    {
		      breakpoint: 600,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 2
		      }
		    },
		    {
		      breakpoint: 480,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1,
		        autoplay: false,
		        arrows: false,
		        adaptiveHeight:true,

		      }
		    },
		     {
		      breakpoint: 360,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    }
		    // You can unslick at a given breakpoint now by adding:
		    // settings: "unslick"
		    // instead of a settings object
		  ]


});


/*if ($(window).width() <= 768) {
	$('.partner-links').slick({
		infinite: true,
		speed: 1500,
		arrows: false,
		autoplay: true,
			responsive: [
		    {
		      breakpoint: 769,
		      settings: {
		        slidesToShow: 3,
		        slidesToScroll: 1,
		        infinite: true,
		        dots: false
		      }
		    },
		    {
		      breakpoint: 600,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 2
		      }
		    },
		    {
		      breakpoint: 480,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 1
		      }
		    },
		     {
		      breakpoint: 360,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    }
		    // You can unslick at a given breakpoint now by adding:
		    // settings: "unslick"
		    // instead of a settings object
		  ]

		 
	});
}*/
$('.call-us a').hover(function(){  
    $(this).text("+61 3 9090 7000");z
}, function() {
    $(this).text("Call us");
});

function movin_the_bar_like_a_boss() {
		jQuery('div.gf_progressbar_wrapper').each(function(i,e){
		    fielddesc = jQuery('<div>').append(jQuery(e).clone()).remove().html();
		    jQuery(e).siblings('div.gform_body').after(fielddesc);
		    jQuery(e).remove();
		});
	}

	movin_the_bar_like_a_boss();

	jQuery(document).bind('gform_page_loaded', function(){
		movin_the_bar_like_a_boss();
	});




//Solution section animation
if ($(window).width() > 768) { 
	$('.row-solution .video').waypoint(function(direction){
		
		$('.row-solution .video').addClass('animated slideInFromRight')
	}, {
		offset: '60%'
	})

	$('.row2-solution .video').waypoint(function(direction){
		$('.row2-solution .video').addClass('animated slideInFromLeft')
	}, {
		offset: '50%'
	})

	$('.row3-solution .video').waypoint(function(direction){
		$('.row3-solution .video').addClass('animated slideInFromRight')
	}, {
		offset: '50%'
	})

	//Contact section amination
 
	$('.banner-content-contact').waypoint(function(direction){
		$('.banner-content-contact').addClass('animated slideInFromLeft')
	}, {
		offset: '80%'
	})

	
	$('.contact-form').waypoint(function(direction){
		$('.contact-form').addClass('animated slideInFromRight')
	}, {
		offset: '80%'
	})

}

//Clients section amination

	$('.group1 .logos').waypoint(function(direction){
		$('.group1 .logos').addClass('animated zoomIn')
	}, {
		offset: '70%'
	})

	$('.group2 .logos').waypoint(function(direction){
		$('.group2 .logos').addClass('animated zoomIn')
	}, {
		offset: '70%'
	})

	$('.group3 .logos').waypoint(function(direction){
		$('.group3 .logos').addClass('animated zoomIn')
	}, {
		offset: '50%'
	})


	$('.group4 .logos').waypoint(function(direction){
		$('.group4 .logos').addClass('animated zoomIn')
	}, {
		offset: '50%'
	})

	



	$('.client-back').waypoint(function(direction){
		$('.client-back').addClass('animated slideInUp')
	}, {
		offset: '50%'
	})

	$("#contact-us").on('click',function() { 
        $('html, body').animate({
            'scrollTop' : $("#contact-panel").position().top
        }, 1500); 
    });

    $(".benefits-menu").on('click',function() { 
        $('html, body').animate({
            'scrollTop' : $("#section2").position().top-100
        }, 1500); 
    });

    $(".solutions").on('click',function() { 
        $('html, body').animate({
            'scrollTop' : $("#section3").position().top-100
        }, 1500); 
    });

    $(".clients-menu").on('click',function() { 
        $('html, body').animate({
            'scrollTop' : $("#section4").position().top-100
        }, 1500); 
    });


     $(".get-started").on('click',function() { 
        $('html, body').animate({
            'scrollTop' : $(".footer").position().top-100
        }, 1500); 
    });

      $(".examples").on('click',function() { 
        $('html, body').animate({
            'scrollTop' : $("#section4").position().top-700
        }, 1500); 
    });



	var	scrolling = false;
	var contentSections = $('.cd-section'),
		verticalNavigation = $('.cd-vertical-nav'),
		navigationItems = verticalNavigation.find('a'),
		navTrigger = $('.cd-nav-trigger'),
		scrollArrow = $('.cd-scroll-down');

	$(window).on('scroll', checkScroll);

	//smooth scroll to the selected section
	verticalNavigation.on('click', 'a', function(event){
        event.preventDefault();
        smoothScroll($(this.hash));
        verticalNavigation.removeClass('open');
    });

    //smooth scroll to the second section
    scrollArrow.on('click', function(event){
    	event.preventDefault();
        smoothScroll($(this.hash));
    });

	// open navigation if user clicks the .cd-nav-trigger - small devices only
    navTrigger.on('click', function(event){
    	event.preventDefault();
    	verticalNavigation.toggleClass('open');
    });

	function checkScroll() {
		if( !scrolling ) {
			scrolling = true;
			(!window.requestAnimationFrame) ? setTimeout(updateSections, 300) : window.requestAnimationFrame(updateSections);
		}
	}

	function updateSections() {
		var halfWindowHeight = $(window).height()/2,
			scrollTop = $(window).scrollTop();
		contentSections.each(function(){
			var section = $(this),
				sectionId = section.attr('id'),
				navigationItem = navigationItems.filter('[href^="#'+ sectionId +'"]');
			( (section.offset().top - halfWindowHeight < scrollTop ) && ( section.offset().top + section.height() - halfWindowHeight > scrollTop) )
				? navigationItem.addClass('active')
				: navigationItem.removeClass('active');
		});
		scrolling = false;
	}

	function smoothScroll(target) {
        $('body,html').animate(
        	{'scrollTop':target.offset().top},
        	300
        );
	}

	//sticky menu


	var cbpAnimatedHeader = (function() {

		var docElem = document.documentElement,
			header = document.querySelector( '.cbp-af-header' ),
			didScroll = false,
			changeHeaderOn = 10;

		function init() {
			window.addEventListener( 'scroll', function( event ) {
				if( !didScroll ) {
					didScroll = true;
					setTimeout( scrollPage, 10 );
				}
			}, false );
		}

		function scrollPage() {
			var sy = scrollY();
			if ( sy >= changeHeaderOn ) {
				classie.add( header, 'cbp-af-header-shrink' );
			}
			else {
				classie.remove( header, 'cbp-af-header-shrink' );
			}
			didScroll = false;
		}

		function scrollY() {
			return window.pageYOffset || docElem.scrollTop;
		}

		init();

	})();

	function scrollToTop() {

		window.scrollTo({top: 0, behavior: 'smooth'});

	}

});