<?php

    /* Template name: HRT Sponsor Forms*/

?>

<html>

<head>
    <title><?php wp_title(''); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" type="text/css" media="all" href="<?php echo (get_stylesheet_directory_uri().'/core/css/old-form-css/hrt_form.css'); ?>" />
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-5333835-9']);
        //_gaq.push(['_trackPageview']);

        (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

    </script>

</head>

<body>
<?php


    if ( have_posts() ) while ( have_posts() ) : the_post(); // start loop

?>
            <div class="hrt-form-wrapper">
            	 <div class="header"><img src="http://e-brochures.com.au/ebmmr2015/wp-content/themes/mmr/images/hrt-form-header.jpg" title="header img"></div>
                 <div class="content-container">
                    <?php $title = preg_split('/<br[^>]*>/i', get_the_title());
						echo '<h1>'.$title[0].'<br><span>'.$title[1].'</span></h1>';
                   		echo the_content(); ?>
                </div>

            </div>
            <div class="footer">
          		<div class="hrt-form-wrapper">
                HOLDEN RACING TEAM<br>
125 RAYHUR STREET<br>
CLAYTON, VICTORIA 3168<br>
					<a class="webadd" href="http://www.hrt.com.au">WWW.HRT.COM.AU</a>
                 </div>
		    </div>


<?php

    endwhile; // end the loop

    //get_footer();
?>

<?php //wp_footer(); ?>

<script>

  var event_name = $('#input_11_37');

  //console.log('event_name', event_name);

  if(event_name.length > 0) {

    var event_price = $('#input_11_18');
    event_price.prop('readonly', true);
    event_price.attr('type', 'text');
    event_price.val('NA');

    event_name.on('change', function () {

      switch(event_name.val()) {
		case 'Winton Supersprint - Winton Motor Raceway, VIC' :
          event_price.attr('type', 'number');
          event_price.val(82.5);
        break;
        case 'Coates Hire Ipswich Supersprint - Queensland Raceway, QLD' :
          event_price.attr('type', 'number');
          event_price.val(65);
        break;
        case 'Perth Supersprint - Barbagallo Raceway, WA' :
          event_price.attr('type', 'number');
          event_price.val(88);
        break;
        case 'Castrol Edge Townsville 400 - Reid Park, QLD' :
          event_price.attr('type', 'number');
          event_price.val(108.75);
        break;
        case 'Clipsal 500 Adelaide - Adelaide Street Circuit, SA' :
          event_price.attr('type', 'number');
          event_price.val(171.75);
        break;
        case 'Tyrepower Tasmania Supersprint - Symmons Plains Raceway, TAS' :
          event_price.attr('type', 'number');
          event_price.val(40);
        break;
        case 'WD-40 Phillip Island Supersprint - Phillip Island Circuit, VIC' :
          event_price.attr('type', 'number');
          event_price.val(57.5);
        break;
        case 'Sydney Motorsport Park Supersprint - Sydney Motorsport Park, NSW' :
          event_price.attr('type', 'number');
          event_price.val(57.5);
        break;
        case 'Wilson Security Sandown 500 - Sandown Raceway, VIC' :
          event_price.attr('type', 'number');
          event_price.val(65);
        break;
        case 'Supercheap Auto Bathurst 1000 - Mount Panorama, NSW' :
          event_price.attr('type', 'number');
          event_price.val(127.5);
        break;
        case 'Castrol Gold Coast 600 - Surfers Paradise, QLD' :
          event_price.attr('type', 'text');
          event_price.val(90);
        break;
        case 'ITM Auckland Supersprint - Pukekohe Park Raceway, NZ' :
          event_price.attr('type', 'text');
          event_price.val(57.47);
        break;
        case 'Coates Hire Sydney 500 - Sydney Olympic Park, NSW' :
          event_price.attr('type', 'text');
          event_price.val(112.5);
        break;
        case '' :
          event_price.attr('type', 'text');
          event_price.val('NA');
        break;
        default :
          event_price.attr('type', 'text');
          event_price.val('TBC');
        break;
      }

    });

  }

</script>

</body>
</html>
