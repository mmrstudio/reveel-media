<?php

    /* Template name: Sydney Thunder Booking Forms*/

?>

<html>

<head>
    <title><?php wp_title(''); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" type="text/css" media="all" href="<?php echo (get_stylesheet_directory_uri().'/core/css/old-form-css/sydney_form.css'); ?>" />
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

</head>

<body>
<?php


    if ( have_posts() ) while ( have_posts() ) : the_post(); // start loop

?>
            <div class="sydney-form-wrapper">
            	 <div class="header"><img src="http://e-brochures.com.au/ebmmr2015/wp-content/themes/mmr/images/Thunder-Booking-Form_01.jpg" title="header img"></div>
                 <div class="content-container">
                    <?php  echo the_content(); ?>
                   		
                </div>

            </div>
            <div class="footer">
          		<div class="sydney-form-wrapper">
                	<a href="http://Sydneythunder.com.au"><img src="http://e-brochures.com.au/ebmmr2015/wp-content/themes/mmr/images/Thunder-Booking-Form_03.jpg" title="Sydneythunder.com.au"></a>
                 </div>
		    </div>


<?php

    endwhile; // end the loop

    //get_footer();
?>

<?php //wp_footer(); ?>

<script>

  var event_name = $('#input_19_72');

  //console.log('event_name', event_name);

  if(event_name.length > 0) {

    var event_price = $('#input_19_79');
	var total = $('#input_19_78');
	
	
	
    event_price.prop('readonly', true);
    event_price.attr('type', 'text');
    event_price.val('NA');

    event_name.on('change', function () {
	  var ticket = $('#input_19_73').val();
	  
      switch(event_name.val()) {
		case 'Private Suite' :
          event_price.attr('type', 'number');
          event_price.val(275);
		  total.val ('$ '+(275*ticket).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,'));;
        break;  
        case 'Private Rooms' :
          event_price.attr('type', 'number');
          event_price.val(160);
		   total.val ('$ '+(160*ticket).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,'));;
        break;
        case 'First Class Seating' :
          event_price.attr('type', 'number');
          event_price.val(65);
		   total.val ('$ '+(65*ticket).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,'));;
        break;
        case 'Governor’s Lounge' :
          event_price.attr('type', 'number');
          event_price.val(300);
		   total.val ('$ '+(300*ticket).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,'));;
        break;
        case 'Skydeck' :
          event_price.attr('type', 'number');
          event_price.val(165);
		   total.val ('$ '+(165*ticket).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,'));;
        break;
        case 'Junior Birthday Party' :
          event_price.attr('type', 'number');
          event_price.val(35);
		   total.val ('$ '+(35*ticket).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,'));;
        break;
        case '' :
          event_price.attr('type', 'text');
          event_price.val('NA');
        break;
        default :
          event_price.attr('type', 'text');
          event_price.val('TBC');
        break;
      }

    });

  }

</script>

</body>
</html>
