<div class="container">
	<div class="clients">
		<div class="title"><?php echo get_field('client_title','option');?> </div>

 
		 
		 <div class="col-sm-12 col-xs-12 client-logo-inner">
	 		<?php
	 		$logos = get_field('client_logos','option');
	 		 
	 		?>
	 		<ul class="partner-links">
	 		<?php foreach($logos as $logo){ ?>

	 			<li> <img src="<?php echo $logo['image'];?>"></li>

	 		<?php }

	 		 ?>
	 		</ul>

	 			<div class="title">What they're saying about us </div>

	 			<div class="testimonial col-sm-12 col-xs-12">
	 				<?php $testimonial = get_field('testimonial','option');

	 				foreach($testimonial as $testimonials){ ?>
	 					<div class="col-md-6 items">
	 						<div class="title-test"><?php echo $testimonials['title']; ?> </div>
	 						<div class="title-desc"><?php echo $testimonials['description']; ?> </div>
	 						<div class="club-logo">
	 							<div class="logo-image"><img src="<?php echo $testimonials['club_logo']; ?>"> </div>
	 							<div class="logo-content"><?php echo $testimonials['club_info']; ?> </div>
	 						 	
	 						</div>
	 					</div>
	 			<?php 	}
	 				?>
	 			</div>
		</div>
<!--  End col-md-5 -->
		

</div>
		<div class="client-back">
		 	<div class="col-md-6">
		 		No matter which industry or  
type of content, eBrochures makes it easy and affordable to deliver documents with real wow-factor.

		 	</div>
		</div>
</div>