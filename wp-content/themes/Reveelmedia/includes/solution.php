<div class="solution-header">
 	<div class="title"><?php echo get_field('solution_title','option');?></div>
	<div class="sub-title"><?php echo get_field('solution_sub_title','option');?></div>

</div>
 <ul class="solution-section">


 	  <!-- Section 1 starts here -->
 <li class="row-solution">
  	
 
	<div class="solution"> 
		<div class="col-lg-6 col-md-7 col-sm-6 col-xs-12">
			<div class="section">
			  	<div class="image"> <img src="<?php echo get_field('s1_image','option');?>"></div>
			  	<div class="right-text">
			  		<div class="sec-title"><?php echo get_field('s1_title','option');?> </div>
			  		<div class="sec-text"> <?php echo get_field('s1_text','option');?></div>
			  	</div>
			  </div>

			</div>

			<div class="col-lg-6 col-md-5 col-sm-6 video col-xs-12">

				<div class="slideInFromRight player">
            		<!-- <iframe width="100%" height="100%" src="<?php bloginfo('stylesheet_directory'); ?>/core/videos/Carlton Shop Clip.mp4" frameborder="0" allowfullscreen></iframe> -->

            		<img src="<?php bloginfo('stylesheet_directory'); ?>/core/images/iphone-analytics.png">
          		</div>

			</div>

	</div>

	
 </li>
 <!-- Section 1 end here -->

 <!-- Section 2 starts here -->
  <li class="row2-solution">
	<div class="solution"> 


			<div class="col-lg-6 col-md-5 col-xs-12 video">

				<div class="slideInFromLeft player">
            		<!-- <iframe width="100%" height="100%" src="<?php bloginfo('stylesheet_directory'); ?>/core/videos/UnitingCare Clip1.mp4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->

            		<img src="<?php bloginfo('stylesheet_directory'); ?>/core/images/ipad-surf.png">
          		</div>

			</div>


		<div class="col-lg-6 col-md-7 col-xs-12">
			<div class="section">
			  	<div class="image"> <img src="<?php echo get_field('s2_image','option');?>"></div>
			  	<div class="right-text">
			  		<div class="sec-title"><?php echo get_field('s2_title','option');?> </div>
			  		<div class="sec-text"> <?php echo get_field('s2_text','option');?></div>
			  	</div>
			</div>

		</div>


	</div>

	 
 </li>
  <!-- Section 2 end here -->

   <!-- Section 3 starts here -->
  <li class="row3-solution">
	<div class="solution"> 

		<div class="col-md-6 col-xs-12 row3-content">
			<div class="section">
			  	<div class="image"> <img src="<?php echo get_field('s3_image','option');?>"></div>
			  	<div class="right-text">
			  		<div class="sec-title"><?php echo get_field('s3_title','option');?> </div>
			  		<div class="sec-text"> <?php echo get_field('s3_text','option');?></div>
			  	</div>
			</div>

		</div>

		<div class="col-md-6 col-xs-12 video">

		    <div class="laptop-wrapper slideInFromRight">
          <!--   <iframe width="100%" height="100%" src="<?php bloginfo('stylesheet_directory'); ?>/core/videos/Tennis Clip.mp4" frameborder="0" allowfullscreen></iframe> -->

            <img src="<?php bloginfo('stylesheet_directory'); ?>/core/images/laptop-share.png">


          </div>

		</div>


	</div>

	 
 </li>
  <!-- Section 3 end here -->


</ul>

<!-- Case studies -->
<div class="case-studies" id="casediv">
<div class="container">


	<div class="title"><?php echo get_field('case_studies_title','option');?> </div>

	<ul class="slides">
	<?php $slides = get_field('e-brochure_slides','option');
				$i = 0;
				foreach($slides as $slide) { 

		 		$i++;?>
			<li> 
				<a href="#mydiv<?php echo $i;?>" class="wplightbox " data-width=800 data-height=400 title="See example">
					<img src="<?php echo $slide['thumb'];?>">
				</a>

				<div id="mydiv<?php echo $i;?>" style="display:none;">
             
       			<p class="divdescription" style="font-size:14px;line-height:20px;">
		            <iframe width="100%" height="100%" frameborder="0" allowfullscreen src="<?php echo $slide['url'];?>"> </iframe>
		        </p>

    	</div>


			</li>
				<!-- Lightbox -->

			
			

		<?php }
	?>
</ul>

</div>
</div>
